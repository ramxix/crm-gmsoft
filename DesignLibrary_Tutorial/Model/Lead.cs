﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace DesignLibrary_Tutorial.Model
{
    
    class Lead
    {
        private int id;
        private string name;
        private string username;
        
        public int Id
        {
            get { return id; }
            set { id = value; }
            
        }
        public string Name
        {
            get { return name; }
            set { name = value; }

        }
        public string Username
        {
            get { return username; }
            set { username = value; }

        }
    }
}