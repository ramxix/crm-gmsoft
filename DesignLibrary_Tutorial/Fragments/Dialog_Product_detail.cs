﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SupportToolbar = Android.Support.V7.Widget.Toolbar;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using Android.Support.V7.App;

namespace DesignLibrary_Tutorial.Fragments
{
    public class Dialog_Product_detail : Android.Support.V4.App.DialogFragment
    {
        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
           
            // Create your fragment here
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            // Use this to return your custom view for this Fragment
            // return inflater.Inflate(Resource.Layout.YourFragment, container, false);
            base.OnCreateView(inflater,container,savedInstanceState);
            var view = inflater.Inflate(Resource.Layout.Dialog_Product,container,false);
            // Create your application here
            SupportToolbar ctoolBar = view.FindViewById<SupportToolbar>(Resource.Id.ctoolbar);
            //numberpicker
            NumberPicker _np = view.FindViewById<NumberPicker>(Resource.Id.numberPicker1);
            
            _np.MinValue = 0;
            _np.MaxValue = 100;
            _np.WrapSelectorWheel = false;
            //setupbuttons
            TextView _cancelbutton = view.FindViewById<TextView>(Resource.Id.cancelbutton);
            TextView _savebutton = view.FindViewById<TextView>(Resource.Id.savebutton);
            TextView _deletebutton = view.FindViewById<TextView>(Resource.Id.deletebutton);
            ImageButton _closebutton = view.FindViewById<ImageButton>(Resource.Id.CloseButton);

            _cancelbutton.Click += delegate { closedialog(); };
            _closebutton.Click += delegate { closedialog(); };

            
            return view;
        }
        public void closedialog()
        {
            Dialog.Dismiss();
        }
    }
}