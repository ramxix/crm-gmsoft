using System;
using Android.OS;
using Android.Support.V7.Widget;
using Android.Views;
using SupportFragment = Android.Support.V4.App.Fragment;
using System.Collections.Generic;
using DesignLibrary_Tutorial.Helpers;
using Android.Graphics;
using Android.Util;
using Android.Content;
using DesignLibrary.Helpers;
using RestSharp;
using DesignLibrary_Tutorial.Model;
using Newtonsoft.Json;
using System.Threading.Tasks;
using static DesignLibrary_Tutorial.Helpers.AdaptersHelper;

namespace DesignLibrary_Tutorial.Fragments
{
    public class Fragment1 : SupportFragment
    {

        List<Lead> response= new List<Lead>();
        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            
            // Create your fragment here
            
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            //ini the view

            RecyclerView recyclerView = inflater.Inflate(Resource.Layout.Fragment1, container, false) as RecyclerView;

            SetUpRecyclerView(recyclerView);
            
            return recyclerView;

        }


        //RECYCLER SETUP
        private async void SetUpRecyclerView(RecyclerView recyclerView)
        {
            
            IRestClient client = new RestClient("http://jsonplaceholder.typicode.com");
            IRestRequest request = new RestRequest("users/", Method.GET);


            // ask for the response to be in JSON syntax
            //request.RequestFormat = DataFormat.Json;

            try
            {
                await Task.Run(() =>
                {
                    response = client.Execute<List<Lead>>(request).Data;
                });

            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.Message);
            }

            recyclerView.SetLayoutManager(new LinearLayoutManager(recyclerView.Context));
            recyclerView.SetAdapter(new SimpleStringRecyclerViewAdapter(recyclerView.Context, response, Activity.Resources));
            recyclerView.SetItemClickListener((rv, position, view) =>
            {
                //An item has been clicked
                Context context = view.Context;
                Intent intent = new Intent(context, typeof(Detail_Lead));
                intent.PutExtra(Detail_Lead.EXTRA_NAME, response[position].Name);

                intent.PutExtra("tmp_obj", JsonConvert.SerializeObject(response[position]));

                context.StartActivity(intent);
            });





        }





       
    }
}
