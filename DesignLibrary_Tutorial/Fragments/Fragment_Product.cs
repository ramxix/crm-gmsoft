﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Support.Design.Widget;
using Android.Util;
using Android.Views;
using Android.Widget;
using DesignLibrary_Tutorial.Model;
using Newtonsoft.Json;
using static DesignLibrary_Tutorial.Helpers.AdaptersHelper;
using SupportFragment = Android.Support.V4.App.Fragment;

namespace DesignLibrary_Tutorial.Fragments
{
    public class Fragment_Product : SupportFragment
    {
        List<Product> _products = new List<Product>();
        ProductsListAdapter2 pla;
        ListView _ls;
        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your fragment here
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            // Use this to return your custom view for this Fragment
            // return inflater.Inflate(Resource.Layout.YourFragment, container, false);
            View view = inflater.Inflate(Resource.Layout.Fragment_product, null, false);
            _ls = view.FindViewById<ListView>(Resource.Id.listView1);

            
            Product p3 = new Product();
            p3.Name = "third product";
            p3.Price = "some price";
            p3.Id = 1;
            
            p3.Id = 3;
           
            _products.Add(p3);

            pla = new ProductsListAdapter2(this.Activity, _products);
            _ls.Adapter = pla;
            _ls.ItemClick += _productItem_itemClick;
            return view;
        }
        public void getNewProducts(String listjson)
        {
            
            _products = JsonConvert.DeserializeObject<List<Product>>(listjson);
            pla.Update(_products);
            System.Diagnostics.Debug.WriteLine(listjson);
        }
        public void _productItem_itemClick(object sender,AdapterView.ItemClickEventArgs e)
        {
            Android.Support.V4.App.FragmentTransaction transaction = FragmentManager.BeginTransaction();
            Dialog_Product_detail _product_dialog = new Dialog_Product_detail();
            _product_dialog.Show(transaction, "product dialog");
            System.Diagnostics.Debug.WriteLine("Im getting clicked");
        }
       
    }
}