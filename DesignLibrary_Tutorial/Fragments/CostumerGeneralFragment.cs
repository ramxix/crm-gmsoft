﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using SupportFragment = Android.Support.V4.App.Fragment;
using Android.Support.V7.App;
using Android.Preferences;

namespace DesignLibrary_Tutorial.Fragments
{
    
    public class CostumerGeneralFragment : SupportFragment
    {
        
        public string loctxt;
        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your fragment here

            


        }
        
        

        //public static GeneralFragment NewInstance()
        //{
        //    var frag1 = new GeneralFragment { Arguments = new Bundle() };
        //    return frag1;
        //}

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            // Use this to return your custom view for this Fragment
            // return inflater.Inflate(Resource.Layout.YourFragment, container, false);

            //var ignored= base.OnCreateView(inflater, container, savedInstanceState);
            View view= inflater.Inflate(Resource.Layout.CostumerGeneralFragment, null,false);
            EditText testbox = view.FindViewById<EditText>(Resource.Id.Nametxtbox);

            Spinner salutationspinner = view.FindViewById<Spinner>(Resource.Id.Salutationspinner);
            Spinner typespinner = view.FindViewById<Spinner>(Resource.Id.Typespinner);
            Spinner Categoryspinner = view.FindViewById<Spinner>(Resource.Id.groupespinner);
            

            var items = new List<string>() { "Mobile", "Work", "Home" };
            
                //String mParam1 = Arguments.GetString("param");
                //int mParam2 = Arguments.GetInt("paramInt");
            

            var adapter = new ArrayAdapter<string>(Activity, Android.Resource.Layout.SimpleSpinnerItem, items);
            
            adapter.SetDropDownViewResource(Android.Resource.Layout.SimpleListItemSingleChoice);
            salutationspinner.Adapter = adapter;
            typespinner.Adapter = adapter;
            Categoryspinner.Adapter = adapter;

            var localclass = Application.Context.GetSharedPreferences("mydata", FileCreationMode.Private);
            testbox.Text = localclass.GetString("json_object", null);
            
            return view;
        }
        public String test()
        {
            EditText testbox = View.FindViewById<EditText>(Resource.Id.Nametxtbox);
            return testbox.Text;
        }
    }
}