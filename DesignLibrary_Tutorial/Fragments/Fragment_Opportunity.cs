﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Support.V7.Widget;
using Android.Util;
using Android.Views;
using Android.Widget;
using SupportFragment = Android.Support.V4.App.Fragment;

namespace DesignLibrary_Tutorial.Fragments
{
    public class Fragment_Opportunity : SupportFragment
    {
        TextView _LeadText;
        String name="Lead";
        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your fragment here
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            // Use this to return your custom view for this Fragment
            // return inflater.Inflate(Resource.Layout.YourFragment, container, false);
            View view = inflater.Inflate(Resource.Layout.Fragment_Opportunity, null,false);

            CardView _LeadCard = view.FindViewById<CardView>(Resource.Id.LeadcardView);
            _LeadText = view.FindViewById<TextView>(Resource.Id.LeadtextView);
            _LeadText.Text = name;
            _LeadCard.Click += delegate
            {
                var myIntent = new Intent(this.Context, typeof(List_Lead));
                myIntent.PutExtra("isret", true);

                StartActivityForResult(myIntent, 2);
            };
            return view;
        }

        public override void OnActivityResult(int requestCode, int resultCode, Intent data)
        {
            base.OnActivityResult(requestCode, resultCode, data);
            if (resultCode == (int)Result.Ok && requestCode == 2)
            {
                name = data.GetStringExtra("leadname");
                _LeadText.Text = name;
                System.Diagnostics.Debug.WriteLine("got it :" + requestCode);
            }

        }
        public void setLeadName(String _LeadName)
        {
            _LeadText.Text = _LeadName;
        }
    }
}