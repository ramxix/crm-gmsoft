using Android.Content;
using Android.OS;
using Android.Views;
using Android.Widget;
using SupportFragment = Android.Support.V4.App.Fragment;
using System;

namespace DesignLibrary_Tutorial.Fragments
{
    public class Fragment_crm : SupportFragment

    {
        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your fragment here
            
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            View view = inflater.Inflate(Resource.Layout.Fragment_crm, container, false);

            ImageButton _CostumerButton = view.FindViewById<ImageButton>(Resource.Id.CostumerButton);
            ImageButton _LeadButton = view.FindViewById<ImageButton>(Resource.Id.LeadButton);
            ImageButton _AppointmentButton = view.FindViewById<ImageButton>(Resource.Id.AppointmentButton);
            ImageButton _OpportunityButton = view.FindViewById<ImageButton>(Resource.Id.OppurtunityButton);

            _CostumerButton.Click += delegate { navigateto(typeof(List_costumer));  };
            _LeadButton.Click += delegate { navigateto(typeof(List_Lead)); };
            _AppointmentButton.Click += delegate { navigateto(typeof(List_appointment)); };
            _OpportunityButton.Click += delegate { navigateto(typeof(List_Opportunity)); };

            return view;
        }

        public void navigateto(Type actClass)
        {
            Intent i = new Intent(this.Context, actClass);
            
            StartActivity(i);
        }
    }
}