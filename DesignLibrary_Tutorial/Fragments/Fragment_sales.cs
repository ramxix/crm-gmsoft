using Android.Content;
using Android.OS;
using Android.Views;
using Android.Widget;
using SupportFragment = Android.Support.V4.App.Fragment;
using System;

namespace DesignLibrary_Tutorial.Fragments
{
    public class Fragment_sales : SupportFragment

    {
        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your fragment here
            
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            View view = inflater.Inflate(Resource.Layout.Fragment_sales, container, false);

            ImageButton _CostumerButton = view.FindViewById<ImageButton>(Resource.Id.CustomerButton);
            ImageButton _ProductButton=view.FindViewById<ImageButton>(Resource.Id.ProductButton);
            ImageButton _OfferButton=view.FindViewById<ImageButton>(Resource.Id.OfferButton);
            
            _CostumerButton.Click += delegate { navigateto(typeof(List_costumer)); };
            _ProductButton.Click += delegate { navigateto(typeof(List_product)); };
            _OfferButton.Click += delegate { navigateto(typeof(Editor_offer)); };
            

            return view;
        }

        public void navigateto(Type actClass)
        {
            Intent i = new Intent(this.Context, actClass);
            
            StartActivity(i);
        }
    }
}