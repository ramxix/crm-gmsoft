﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Support.V7.App;
using Android.Support.V7.Widget;

using SupportToolbar = Android.Support.V7.Widget.Toolbar;
using DesignLibrary_Tutorial.Model;
using DesignLibrary_Tutorial.Helpers;
using Android.Support.Design.Widget;
using Newtonsoft.Json;
using static DesignLibrary_Tutorial.Helpers.AdaptersHelper;

namespace DesignLibrary_Tutorial
{
    [Activity(Label = "List_product", Theme = "@style/Theme.DesignDemo")]
    public class List_product : AppCompatActivity, ISelectedChecker
    {
        ListView _ls;
        List<Product> _products = new List<Product>();
        List<Product> _selectedProducts = new List<Product>();
        FloatingActionButton fab;
        private ProductsListAdapter _productsListAdapter;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.List_product);
            // Create your application here
            SupportToolbar ctoolBar = FindViewById<SupportToolbar>(Resource.Id.ctoolbar);
            SetSupportActionBar(ctoolBar);
            SupportActionBar.SetDisplayHomeAsUpEnabled(true);
            //Setup list
            _ls = FindViewById<ListView>(Resource.Id.listView1);
            Product p1 = new Product();
            Product p2 = new Product();
            Product p3 = new Product();
            p3.Name = "third product";
            p3.Price = "some price";
            p3.Id = 1;
            p2.Name = "leather";
            p2.Price = "600";
            p2.Id = 2;
            p1.Price = "1000";
            p1.Name = "Beans and meat";
            p3.Id = 3;
            _products.Add(p2);
            _products.Add(p1);
            _products.Add(p3);
           

            _ls.ItemLongClick += _ProductsListView_ItemLongClick;

            //fab
             fab = FindViewById<FloatingActionButton>(Resource.Id.fab);
            //adapter
            _productsListAdapter = new ProductsListAdapter(this, _products, this);
            _ls.Adapter = _productsListAdapter;

            fab.Visibility = ViewStates.Invisible;

            fab.Click += (o, e) =>
            {
                Intent myIntent = new Intent(this, typeof(Detail_Opportunity));
                myIntent.PutExtra("selectedproducts", SerializeSelectedElements(_selectedProducts));
                SetResult(Result.Ok, myIntent);
                Finish();
            };


        }
        internal String SerializeSelectedElements(List<Product> _list)
        {
            //serialize list of selected products to json
            String _json = JsonConvert.SerializeObject(_list);
            return _json;
        }
        public bool IsItemSelected(int id)
        {
            return _selectedProducts.Select(x => x.Id).Contains(id);
        }
        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case Android.Resource.Id.Home:
                    Finish();
                    return true;

            }

            return base.OnOptionsItemSelected(item);
        }

        private void _ProductsListView_ItemLongClick(object sender, AdapterView.ItemLongClickEventArgs e)
        {
            int id = (int)e.Id;
            View view = e.View;

            RelativeLayout rl = view.FindViewById<RelativeLayout>(Resource.Id.relativelayout1);
            System.Diagnostics.Debug.WriteLine("id:"+id+" selected?:"+ _selectedProducts.Select(x => x.Id).Contains(id));
            if (_selectedProducts.Select(x => x.Id).Contains(id))
            {
                // deselect element
                var colorForUnselected = Resources.GetString(Resource.Color.listitemunselected);
                rl.SetBackgroundColor(Android.Graphics.Color.ParseColor(colorForUnselected));
                _selectedProducts.Remove(_productsListAdapter[e.Position]);
                //hide valide button
                if (_selectedProducts.Count == 0)
                {
                    fab.Visibility = ViewStates.Invisible;
                }

            }
            else
            {
                if (_selectedProducts.Count == 0)
                {
                    fab.Visibility = ViewStates.Visible;
                }
                // select element
                var colorForSelected = Resources.GetString(Resource.Color.listitemselected);
                rl.SetBackgroundColor(Android.Graphics.Color.ParseColor(colorForSelected));
                _selectedProducts.Add(_productsListAdapter[e.Position]);

              
            }

            
        }
        protected void UnselectElements()
        {
            int count = _ls.ChildCount;

            for (int i = 0; i < count; i++)
            {
                View row = _ls.GetChildAt(i);
                var rl = row.FindViewById<RelativeLayout>(Resource.Id.relativelayout1);
                var color = Resources.GetString(Resource.Color.listitemunselected);
                rl.SetBackgroundColor(Android.Graphics.Color.ParseColor(color));
            }
        }



    }
}