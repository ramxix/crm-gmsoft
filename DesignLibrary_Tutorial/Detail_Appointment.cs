﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using SupportToolbar = Android.Support.V7.Widget.Toolbar;
using Android.Views;
using Android.Widget;
using Android.Support.V7.App;
using Android.Support.V7.Widget;
using DatePickerDialog = Com.Wdullaer.Materialdatetimepicker.Date.DatePickerDialog;
using TimePickerDialog = Com.Wdullaer.Materialdatetimepicker.Time.TimePickerDialog;
using Android.Icu.Util;
using Com.Wdullaer.Materialdatetimepicker.Date;
using Android.Icu.Text;
using Com.Wdullaer.Materialdatetimepicker.Time;

namespace DesignLibrary_Tutorial
{
    [Activity(Label = "Detail_Appointment",Theme = "@style/Theme.DesignDemo")]
    public class Detail_Appointment : AppCompatActivity,DatePickerDialog.IOnDateSetListener,TimePickerDialog.IOnTimeSetListener
    {
        int flag = -1;
        TextView _starttext;
        TextView _endtext;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your application here
            SetContentView(Resource.Layout.Detail_Appointment);

            SupportToolbar ctoolBar = FindViewById<SupportToolbar>(Resource.Id.ctoolbar);
            SetSupportActionBar(ctoolBar);
            SupportActionBar.SetDisplayHomeAsUpEnabled(true);
            //setup TextViews
            _starttext = FindViewById<TextView>(Resource.Id.starttext);
            _endtext = FindViewById<TextView>(Resource.Id.endtext);
            //setup button*
            RelativeLayout _startLayout = FindViewById<RelativeLayout>(Resource.Id.startLayout);
            RelativeLayout _endLayout = FindViewById<RelativeLayout>(Resource.Id.endLayout);

            _startLayout.Click += delegate {
                flag = 0;
                getDateTime("Start Date","start");
                
            };
            _endLayout.Click += delegate
            {
                flag = 1;
                getDateTime("End Date","end");
            };
        }
        public void getDateTime(String title,String tag)
        {
            Calendar now = Calendar.Instance;
            DatePickerDialog datepicker = DatePickerDialog.NewInstance(this, now.Get(CalendarField.Year), now.Get(CalendarField.Month), now.Get(CalendarField.DayOfMonth));
            
            datepicker.SetTitle(title);
            datepicker.Show(FragmentManager, tag);
            //06 Apri,2018

        }
        public void getTime(String Title,String tag)
        {
            Calendar now = Calendar.Instance;
            TimePickerDialog timepicker = TimePickerDialog.NewInstance(this, now.Get(CalendarField.HourOfDay), now.Get(CalendarField.Minute),true);
            timepicker.Title = Title;
            timepicker.Show(FragmentManager,tag);
            
        }

        public void OnDateSet(DatePickerDialog p0, int Year, int Month, int DayOfMonth)
        {
            String Mo = getMonth(Month);
            //System.Diagnostics.Debug.WriteLine($"you have selecte: {Year}/{Mo}/{DayOfMonth}");
            String date = $"{DayOfMonth} {Mo},{Year}";
            
            if (flag==0) { 
            _starttext.Text = date;
                
                getTime("Start Time","start");
                
            }
            else if (flag==1)
            {
                _endtext.Text = date;
                
                getTime("End Time", "end");

            }
            else
            {
                System.Diagnostics.Debug.WriteLine("error on date picker");
            }

            

        }
        public void OnTimeSet(RadialPickerLayout p0, int hour, int minute, int p3)
        {
            //RadialPickerLayout
            String Time = $" {string.Format("{0:00}",hour)}:{string.Format("{0:00}",minute)}";
            
            if (flag == 0)
            {
                _starttext.Text += Time;
            }
            else if (flag==1)
            {
                _endtext.Text += Time;
            }
            else
            {
                System.Diagnostics.Debug.WriteLine("error on date picker");
            }
        }

        public String getMonth(int month)
        {
            return new DateFormatSymbols().GetMonths()[month];
        }

        
    }
}