﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using SupportToolbar = Android.Support.V7.Widget.Toolbar;
using Android.Support.V7.App;
using DesignLibrary_Tutorial.Model;
using Android.Support.V7.Widget;
using RestSharp;
using DesignLibrary.Helpers;
using Newtonsoft.Json;
using FloatingSearchViews;
using RestSharp.Deserializers;
using System.Threading.Tasks;
using Com.Wang.Avi;
using static DesignLibrary_Tutorial.Helpers.AdaptersHelper;

namespace DesignLibrary_Tutorial
{
    [Activity(Label = "Lead list", Theme ="@style/Theme.DesignDemo")]
    public class List_Lead : AppCompatActivity
    {
        List<Lead> response=new List<Lead>();
        RecyclerView.Adapter adapter;
        private AVLoadingIndicatorView licon;
        public bool isForResult = false; 

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.List_lead);


            // Create your application here
            SupportToolbar ctoolBar = FindViewById<SupportToolbar>(Resource.Id.ctoolbar);
            SetSupportActionBar(ctoolBar);
            SupportActionBar.SetDisplayHomeAsUpEnabled(true);
            //loading icon
            licon = FindViewById<AVLoadingIndicatorView>(Resource.Id.loadingicon);
            licon.Show();
            //get return mode
            isForResult = Intent.GetBooleanExtra("isret",false);

            RecyclerView recyclerView = FindViewById<RecyclerView>(Resource.Id.recyclerview);

            SetUpRecyclerView(recyclerView);

            FloatingSearchView searchView = FindViewById<FloatingSearchView>(Resource.Id.floating_search_view);

            

        }


        private async void SetUpRecyclerView(RecyclerView recyclerView)
        {

            IRestClient client = new RestClient("http://jsonplaceholder.typicode.com");
            IRestRequest request = new RestRequest("users/", Method.GET);


            // ask for the response to be in JSON syntax
            //request.RequestFormat = DataFormat.Json;
            
            
            

            try
            {
                await Task.Run(() =>
                {
                    response = client.Execute<List<Lead>>(request).Data;

                });
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.Message);
            }
            //bool isEmpty = !response.Any();

            if (response != null)
            {
                
                recyclerView.SetLayoutManager(new LinearLayoutManager(recyclerView.Context));
                RecyclerView.Adapter adapter = new SimpleStringRecyclerViewAdapter(recyclerView.Context, response, Resources);
                recyclerView.SetAdapter(adapter);


                recyclerView.SetItemClickListener((rv, position, view) =>
                {
                    if (isForResult==false) { 
                    //An item has been clicked
                    Context context = view.Context;
                    Intent intent = new Intent(context, typeof(Detail_Lead));
                    intent.PutExtra(Detail_Lead.EXTRA_NAME, response[position].Name);

                    intent.PutExtra("tmp_obj", JsonConvert.SerializeObject(response[position]));

                    context.StartActivity(intent);
                    }
                    else
                    {
                        Intent myIntent = new Intent(this, typeof(Detail_Opportunity));
                        myIntent.PutExtra("leadname", response[position].Name);
                        SetResult(Result.Ok, myIntent);
                        Finish();
                    }
                });
                //hide the loading icon
                licon.SmoothToHide();
            }

        }


        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case Android.Resource.Id.Home:
                    Finish();
                    return true;

            }

            return base.OnOptionsItemSelected(item);
        }
    }
}