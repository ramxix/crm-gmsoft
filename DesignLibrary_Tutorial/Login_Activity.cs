﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Support.V7.Widget;

namespace DesignLibrary_Tutorial
{
    [Activity(Label = "Login", MainLauncher = true,Theme = "@style/Theme.DesignDemo")]
    public class Login_Activity : Activity
    {
        private bool settings;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            settings = true;
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.Login);

            // Create your application here
            //opening the main frame
            var maincard = FindViewById<CardView>(Resource.Id.servercard);
            ImageButton settingButton = FindViewById<ImageButton>(Resource.Id.SettingButton);
           Button LoginButton =(Button)FindViewById(Resource.Id.loginbutton1);

            LoginButton.Click += delegate { loginbtnClick(); };
            settingButton.Click += delegate {
                Android.Animation.ObjectAnimator animatorX;
                if (settings== true) { 
                 animatorX = Android.Animation.ObjectAnimator.OfFloat(maincard, "TranslationX", -980f);
                    settings = false;
                }
                else
                {
                  animatorX = Android.Animation.ObjectAnimator.OfFloat(maincard, "TranslationX", 0f);
                    settings = true;
                }
                animatorX.SetDuration(300);
                animatorX.Start();
            };
        }
        public void loginbtnClick()
        {
            Intent i = new Intent(this, typeof(MainActivity));
            StartActivity(i);
        }
    }
}