﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using DesignLibrary_Tutorial.Model;
using Android.Content.Res;
using Android.Util;
using Android.Support.V7.Widget;
using Android.Graphics;

namespace DesignLibrary_Tutorial.Helpers
{
    class AdaptersHelper
    {
        public class ProductsListAdapter : BaseAdapter<Product>
        {
            private Activity _context;
            private List<Product> _products;
            private ISelectedChecker _selectedChecker;

            public ProductsListAdapter(Activity context, List<Product> products, ISelectedChecker selectedChecker)
            {
                _context = context;
                _products = products;
                _selectedChecker = selectedChecker;
            }


            public override Product this[int position] => _products[position];

            public override int Count => _products.Count;

            public override long GetItemId(int position)
            {
                return this[position].Id;
            }

            public override View GetView(int position, View convertView, ViewGroup parent)
            {
                var item = this[position];

                if (convertView == null)
                {
                    convertView = _context.LayoutInflater.Inflate(Resource.Layout.Row_product, null);
                }
                else
                {
                    var id = (int)GetItemId(position);
                    RelativeLayout rl = convertView.FindViewById<RelativeLayout>(Resource.Id.relativelayout1);

                    if (_selectedChecker.IsItemSelected(id))
                    {
                        var colorForSelected = _context.Resources.GetString(Resource.Color.listitemselected);
                        rl.SetBackgroundColor(Android.Graphics.Color.ParseColor(colorForSelected));
                    }
                    else
                    {
                        var colorForUnselected = _context.Resources.GetString(Resource.Color.listitemunselected);
                        rl.SetBackgroundColor(Android.Graphics.Color.ParseColor(colorForUnselected));
                    }
                }

                convertView.FindViewById<TextView>(Resource.Id.text1).Text = item.Name;
                convertView.FindViewById<TextView>(Resource.Id.sectext1).Text = item.Price;




                return convertView;
            }
        }

        public class ProductsListAdapter2 : BaseAdapter<Product>
        {
            private Activity _context;
            private List<Product> _products;
           

            public ProductsListAdapter2(Activity context, List<Product> products)
            {
                _context = context;
                _products = products;
               
            }


            public override Product this[int position] => _products[position];

            public override int Count => _products.Count;

            public override long GetItemId(int position)
            {
                return this[position].Id;
            }

            public override View GetView(int position, View convertView, ViewGroup parent)
            {

                var item = this[position];
                convertView = _context.LayoutInflater.Inflate(Resource.Layout.Row_product, null);

                convertView.FindViewById<TextView>(Resource.Id.text1).Text = item.Name;
                convertView.FindViewById<TextView>(Resource.Id.sectext1).Text = item.Price;

                return convertView;
            }
            public void Update(List<Product> mLists)
            {
                
                _products.Clear();
                _products.AddRange(mLists);
                NotifyDataSetChanged();
            }
        }

        public class SimpleStringRecyclerViewAdapter : RecyclerView.Adapter
        {
            private readonly TypedValue mTypedValue = new TypedValue();
            private int mBackground;
            private List<Lead> mResponse;
            Resources mResource;
            private Dictionary<int, int> mCalculatedSizes;
            //Constr
            internal SimpleStringRecyclerViewAdapter(Context context, List<Lead> response, Resources res)
            {
                context.Theme.ResolveAttribute(Resource.Attribute.selectableItemBackground, mTypedValue, true);
                mBackground = mTypedValue.ResourceId;
                mResponse = response;
                mResource = res;

                mCalculatedSizes = new Dictionary<int, int>();
            }

            public override int ItemCount
            {
                get
                {
                    return mResponse.Count;
                }
            }

            public override async void OnBindViewHolder(RecyclerView.ViewHolder holder, int position)
            {

                var simpleHolder = holder as SimpleViewHolder;
                //you should change this
                simpleHolder.mBoundString = mResponse[position].Name;
                simpleHolder.mTxtView.Text = mResponse[position].Name;
                simpleHolder.mSecTxtView.Text = mResponse[position].Username;
                //Random photoes of cheeses hhhh
                int drawableID = Cheeses.RandomCheeseDrawable;
                BitmapFactory.Options options = new BitmapFactory.Options();
                //resolution shinanigan
                if (mCalculatedSizes.ContainsKey(drawableID))
                {
                    options.InSampleSize = mCalculatedSizes[drawableID];
                }

                else
                {
                    options.InJustDecodeBounds = true;

                    BitmapFactory.DecodeResource(mResource, drawableID, options);

                    options.InSampleSize = Cheeses.CalculateInSampleSize(options, 100, 100);
                    options.InJustDecodeBounds = false;

                    mCalculatedSizes.Add(drawableID, options.InSampleSize);
                }


                var bitMap = await BitmapFactory.DecodeResourceAsync(mResource, drawableID, options);

                simpleHolder.mImageView.SetImageBitmap(bitMap);
            }

            public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType)
            {
                //this is probably gonna change too
                View view = LayoutInflater.From(parent.Context).Inflate(Resource.Layout.List_Item, parent, false);
                view.SetBackgroundResource(mBackground);

                return new SimpleViewHolder(view);
            }
        }
        //very important class
        public class SimpleViewHolder : RecyclerView.ViewHolder
        {
            public string mBoundString;
            public readonly View mView;
            public readonly ImageView mImageView;
            public readonly TextView mTxtView;
            public readonly TextView mSecTxtView;
            public SimpleViewHolder(View view) : base(view)
            {
                mView = view;
                mImageView = view.FindViewById<ImageView>(Resource.Id.avatar);
                mTxtView = view.FindViewById<TextView>(Resource.Id.text1);
                mSecTxtView = view.FindViewById<TextView>(Resource.Id.sectext1);
            }

            public override string ToString()
            {
                return base.ToString() + " '" + mTxtView.Text;
            }
        }
        //ACTIVITY ADAPTER
        public class ActivityRecyclerViewAdapter : RecyclerView.Adapter
        {
            private readonly TypedValue mTypedValue = new TypedValue();
            private int mBackground;
            private List<Activity_model> mResponse;
            Resources mResource;
            private Dictionary<int, int> mCalculatedSizes;
            //Constr
            internal ActivityRecyclerViewAdapter(Context context, List<Activity_model> response, Resources res)
            {
                context.Theme.ResolveAttribute(Resource.Attribute.selectableItemBackground, mTypedValue, true);
                mBackground = mTypedValue.ResourceId;
                mResponse = response;
                mResource = res;

                mCalculatedSizes = new Dictionary<int, int>();
            }

            public override int ItemCount
            {
                get
                {
                    return mResponse.Count;
                }
            }

            public override async void OnBindViewHolder(RecyclerView.ViewHolder holder, int position)
            {

                var simpleHolder = holder as ActivityViewHolder;
                //you should change this
                simpleHolder.mBoundString = mResponse[position].Name;
                simpleHolder.mTxtView.Text = mResponse[position].Name;
                simpleHolder.mSecTxtView.Text = mResponse[position].Date;




            }

            public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType)
            {
                //this is probably gonna change too
                View view = LayoutInflater.From(parent.Context).Inflate(Resource.Layout.List_Item_activities, parent, false);
                view.SetBackgroundResource(mBackground);

                return new ActivityViewHolder(view);
            }
        }
        
        public class ActivityViewHolder : RecyclerView.ViewHolder
        {
            public string mBoundString;
            public readonly View mView;
            //public readonly ImageView mIconView;
            public readonly TextView mTxtView;
            public readonly TextView mSecTxtView;
            public ActivityViewHolder(View view) : base(view)
            {
                mView = view;
                //mIconView = view.FindViewById<ImageView>(Resource.Id.imageView1);
                mTxtView = view.FindViewById<TextView>(Resource.Id.text1);
               mSecTxtView = view.FindViewById<TextView>(Resource.Id.sectext1);
            }

            public override string ToString()
            {
                return base.ToString() + " '" + mTxtView.Text;
            }
        }
    }
}