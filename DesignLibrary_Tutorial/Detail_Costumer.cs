﻿
using Android.App;
using Android.OS;
using SupportToolbar = Android.Support.V7.Widget.Toolbar;
using Android.Support.V7.App;
using Android.Support.Design.Widget;
using System;
using Android.Widget;
using DesignLibrary_Tutorial.Helpers;
using Android.Views;
using Android.Content;
using DesignLibrary_Tutorial.Model;

namespace DesignLibrary_Tutorial
{
    [Activity(Label = "Detail Costumer", Theme = "@style/Theme.DesignDemo")]
    internal class Detail_Costumer : AppCompatActivity
    {
        public const string EXTRA_NAME = "cheese_name";
        private string tmp_obj;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.Detail_Costumer);

            SupportToolbar toolBar = FindViewById<SupportToolbar>(Resource.Id.toolbar);
            SetSupportActionBar(toolBar);
            SupportActionBar.SetDisplayHomeAsUpEnabled(true);

            string cheeseName = Intent.GetStringExtra(EXTRA_NAME);
            tmp_obj = Intent.GetStringExtra("tmp_obj");
            System.Diagnostics.Debug.WriteLine(tmp_obj);

            CollapsingToolbarLayout collapsingToolBar = FindViewById<CollapsingToolbarLayout>(Resource.Id.collapsing_toolbar);
            collapsingToolBar.Title = cheeseName;

            LoadBackDrop();
            FloatingActionButton morefab = FindViewById<FloatingActionButton>(Resource.Id.moreactionbutton);
            morefab.Click += delegate
            {
                Client_Edit();
            };


        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case Android.Resource.Id.Home:
                    Finish();
                    return true;
                case Resource.Id.action_edit:
                    Client_Edit();
                    return true;
            }

            return base.OnOptionsItemSelected(item);
        }

        public override bool OnCreateOptionsMenu(IMenu menu)
        {
            MenuInflater.Inflate(Resource.Menu.sample_actions, menu);
            return base.OnCreateOptionsMenu(menu);
        }
        public void Client_Edit()
        {
            Intent i = new Intent(this, typeof(Editor_costumer));
            i.PutExtra("tmp_obj", tmp_obj);
            StartActivity(i);
        }

        private void LoadBackDrop()
        {
            ImageView imageView = FindViewById<ImageView>(Resource.Id.backdrop);
            imageView.SetImageResource(Cheeses.RandomCheeseDrawable);
        }
    }
}