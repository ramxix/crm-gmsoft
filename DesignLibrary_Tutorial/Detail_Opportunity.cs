﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Support.V7.App;
using SupportToolbar = Android.Support.V7.Widget.Toolbar;
using BottomNavigationBar;
using DesignLibrary_Tutorial.Fragments;
using BottomNavigationBar.Listeners;
using Android.Support.Design.Widget;
using Android.Support.V7.Widget;

namespace DesignLibrary_Tutorial
{
    [Activity(Label = "Detail_Opportunity", Theme = "@style/Theme.DesignDemo")]
    public class Detail_Opportunity : AppCompatActivity, IOnMenuTabClickListener
    {
        private BottomBar _bottomBar;
        FloatingActionButton fab;
        public const String EXTRA_NAME = "opportunity_name";
        Android.Support.V4.App.Fragment fragment4 = new Fragment_Opportunity();
        Android.Support.V4.App.Fragment fragment5 = new Fragment_Product();
       
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your application here
            SetContentView(Resource.Layout.Detail_Opportunity);
            //toolbar
            SupportToolbar ctoolBar = FindViewById<SupportToolbar>(Resource.Id.ctoolbar);
            SetSupportActionBar(ctoolBar);
            SupportActionBar.SetDisplayHomeAsUpEnabled(true);
            //fab
            fab=FindViewById<FloatingActionButton>(Resource.Id.fab);
            // bottom navigation bar
            _bottomBar = BottomBar.Attach(this, savedInstanceState);
            _bottomBar.SetItems(Resource.Menu.Bottom_Opportunity);
            _bottomBar.SetOnMenuTabClickListener(this);

            string opportunityName = Intent.GetStringExtra(EXTRA_NAME);
            ctoolBar.Title = opportunityName;



            fab.Click += (o, e) =>
            {
                //load the "select product" activity
                Intent i = new Intent(this, typeof(List_product));
                i.PutExtra("isret", true);
                StartActivityForResult(i,1);
               
            };
        }

        protected override void OnActivityResult(int requestCode, Result resultCode, Intent data)
        {
            base.OnActivityResult(requestCode, resultCode, data);

            if (resultCode == Result.Ok)
            {
                //if return is comming from the products list
                if (requestCode == 1)
                {
                    Fragment_Product fp = (Fragment_Product) fragment5;
                    
                    fp.getNewProducts(data.GetStringExtra("selectedproducts"));
                    
                }

             
            }

            }


    #region IOnMenuTabClickListener implementation
    public void OnMenuTabSelected(int menuItemId)
        {
            LoadFragment(menuItemId);
        }
        public void OnMenuTabReSelected(int menuItemId)
        {
            return;
        }
   #endregion
        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case Android.Resource.Id.Home:
                    Finish();
                    return true;

            }

            return base.OnOptionsItemSelected(item);
        }
        protected override void OnSaveInstanceState(Bundle outState)
        {
            base.OnSaveInstanceState(outState);

            // Necessary to restore the BottomBar's state, otherwise we would
            // lose the current tab on orientation change.
            _bottomBar.OnSaveInstanceState(outState);
        }
        void LoadFragment(int id)
        {

            switch (id)
            {


                case Resource.Id.GenralTab:
                    replacefragment(fragment4);
                    fab.Visibility = ViewStates.Invisible;
                    
                    break;
                case Resource.Id.ProductTab:
                    //show the fab only when your in the products section
                    replacefragment(fragment5);
                    fab.Visibility = ViewStates.Visible;
                   
                    break;
                

                default:
                    Toast.MakeText(this, "Somthing went Wrong!", ToastLength.Long).Show();
                    break;
                    

            }
            if (fragment4 == null)
                return;

            return;

        }
      
        private void replacefragment(Android.Support.V4.App.Fragment frag)
        {
            SupportFragmentManager.BeginTransaction().Replace(Resource.Id.content_frame, frag).Commit();
        }
    }
}