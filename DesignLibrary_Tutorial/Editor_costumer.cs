﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using SupportToolbar = Android.Support.V7.Widget.Toolbar;
using BottomNavigationBar;
using Android.Support.V7.App;

using BottomNavigationBar.Listeners;
using DesignLibrary_Tutorial.Fragments;
using Android.Preferences;

namespace DesignLibrary_Tutorial
{
    [Activity(Label = "Editor_Costumer_Activity", Theme = "@style/Theme.DesignDemo")]
    public class Editor_costumer : AppCompatActivity, IOnMenuTabClickListener
    {
        
        private BottomBar _bottomBar;
        public Context mContext;
        public string tmp_obj;
        Android.Support.V4.App.Fragment fragment = new CostumerGeneralFragment();
        Android.Support.V4.App.Fragment fragment2 = new AddressFragment();
        Android.Support.V4.App.Fragment fragment3 = new ContactsFragment();
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.Editor_Costumer);
           
            // Create your application here
            SupportToolbar ctoolBar = FindViewById<SupportToolbar>(Resource.Id.ctoolbar);
            SetSupportActionBar(ctoolBar);
            SupportActionBar.SetDisplayHomeAsUpEnabled(true);
            
            // bottom navigation bar
            _bottomBar = BottomBar.Attach(this, savedInstanceState);
            _bottomBar.SetItems(Resource.Menu.Bottom_Clientedit);
            _bottomBar.SetOnMenuTabClickListener(this);

            //getjsonfromactivity
            tmp_obj = Intent.GetStringExtra("tmp_obj");
            //share Lead Class
            var localclass= Application.Context.GetSharedPreferences("mydata",FileCreationMode.Private);
            var classeditor = localclass.Edit();
            classeditor.PutString("json_object", tmp_obj);           
            classeditor.Apply();

            


        }
        
        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case Android.Resource.Id.Home:
                    Finish();
                    return true;
                case Resource.Id.action_ok:
                    action_ok();
                    return true;

            }

            return base.OnOptionsItemSelected(item);
        }

       public void action_ok()
        {
            CostumerGeneralFragment tetfra = (CostumerGeneralFragment)fragment;
           
            System.Diagnostics.Debug.WriteLine(tetfra.test());


        }
        public override bool OnCreateOptionsMenu(IMenu menu)
        {
            MenuInflater.Inflate(Resource.Menu.Editor_actions, menu);
            return base.OnCreateOptionsMenu(menu);
        }


        #region IOnMenuTabClickListener implementation
        public void OnMenuTabSelected(int menuItemId)
        {
            LoadFragment(menuItemId);
        }
        public void OnMenuTabReSelected(int menuItemId)
        {
            return;
        }

        

        #endregion


        protected override void OnSaveInstanceState(Bundle outState)
        {
            base.OnSaveInstanceState(outState);

            // Necessary to restore the BottomBar's state, otherwise we would
            // lose the current tab on orientation change.
            _bottomBar.OnSaveInstanceState(outState);
        }

        void LoadFragment(int id)
        {
            
            switch (id)
            {


                case Resource.Id.GenralTab:

                    replacefragment( fragment);
                    break;
                case Resource.Id.MainaddressTab:

                    replacefragment(fragment2);
                    break;
                case Resource.Id.ContactsTab:
                    replacefragment(fragment3);
                    break;
                default:
                    Toast.MakeText(this, "Somthing went Wrong!", ToastLength.Long).Show();
                    break;
                    //Bundle args = new Bundle();
                    //args.PutString("param", "sometimes");
                    //args.PutInt("paramInt", 12);
                    //fragment.Arguments=args;

            }
            if (fragment == null)
                return;

            return;

            
        }
        private void replacefragment( Android.Support.V4.App.Fragment frag)
        {
            SupportFragmentManager.BeginTransaction().Replace(Resource.Id.content_frame, frag).Commit();
        }
    }
}