﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Support.V7.AppCompat;
using Android.Support.V7.App;
using Android.Support.V7.Widget;
using Com.Wang.Avi;
using SupportToolbar = Android.Support.V7.Widget.Toolbar;
using FloatingSearchViews;
using RestSharp;
using System.Threading.Tasks;
using DesignLibrary_Tutorial.Model;
using DesignLibrary.Helpers;
using static DesignLibrary_Tutorial.Helpers.AdaptersHelper;
using Newtonsoft.Json;

namespace DesignLibrary_Tutorial
{
    [Activity(Label = "List Opportunities", Theme = "@style/Theme.DesignDemo")]
    public class List_Opportunity : AppCompatActivity
    {
        List<Lead> response = new List<Lead>();
        RecyclerView.Adapter adapter;
        private AVLoadingIndicatorView licon;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.List_opportunitiy);


            // Create your application here
            SupportToolbar ctoolBar = FindViewById<SupportToolbar>(Resource.Id.ctoolbar);
            SetSupportActionBar(ctoolBar);
            SupportActionBar.SetDisplayHomeAsUpEnabled(true);
            //loading icon
            licon = FindViewById<AVLoadingIndicatorView>(Resource.Id.loadingicon);
            licon.Show();
            RecyclerView recyclerView = FindViewById<RecyclerView>(Resource.Id.recyclerview);
            SetUpRecyclerView(recyclerView);
            FloatingSearchView searchView = FindViewById<FloatingSearchView>(Resource.Id.floating_search_view);
        }
        private async void SetUpRecyclerView(RecyclerView recyclerView)
        {

            IRestClient client = new RestClient("http://jsonplaceholder.typicode.com");
            IRestRequest request = new RestRequest("users/", Method.GET);


            // ask for the response to be in JSON syntax
            //request.RequestFormat = DataFormat.Json;




            try
            {
                await Task.Run(() =>
                {
                    response = client.Execute<List<Lead>>(request).Data;

                });
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.Message);
            }
            //bool isEmpty = !response.Any();

            if (response != null)
            {

                recyclerView.SetLayoutManager(new LinearLayoutManager(recyclerView.Context));
                RecyclerView.Adapter adapter = new SimpleStringRecyclerViewAdapter(recyclerView.Context, response, Resources);
                recyclerView.SetAdapter(adapter);


                recyclerView.SetItemClickListener((rv, position, view) =>
                {
                    //An item has been clicked
                    Context context = view.Context;
                    Intent intent = new Intent(context, typeof(Detail_Opportunity));
                    intent.PutExtra(Detail_Opportunity.EXTRA_NAME, response[position].Name);

                    intent.PutExtra("tmp_obj", JsonConvert.SerializeObject(response[position]));

                    context.StartActivity(intent);
                });
                //hide the loading icon
                licon.SmoothToHide();
            }

        }


        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case Android.Resource.Id.Home:
                    Finish();
                    return true;

            }

            return base.OnOptionsItemSelected(item);
        }
    }
}