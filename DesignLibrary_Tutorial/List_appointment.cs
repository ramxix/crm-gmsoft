﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Support.V7.App;
using Android.Support.V7.Widget;
using DesignLibrary_Tutorial.Model;
using SupportToolbar = Android.Support.V7.Widget.Toolbar;

using static DesignLibrary_Tutorial.Helpers.AdaptersHelper;
using DesignLibrary.Helpers;
using Newtonsoft.Json;

namespace DesignLibrary_Tutorial
{
    [Activity(Label = "Appointment list", Theme = "@style/Theme.DesignDemo")]
    public class List_appointment : AppCompatActivity
    {
        List<Activity_model> response = new List<Activity_model>();
        RecyclerView.Adapter adapter;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.List_activities);

            SupportToolbar ctoolBar = FindViewById<SupportToolbar>(Resource.Id.ctoolbar);
            SetSupportActionBar(ctoolBar);
            SupportActionBar.SetDisplayHomeAsUpEnabled(true);
            // Create your application here
            RecyclerView recyclerView = FindViewById<RecyclerView>(Resource.Id.recyclerView);
            Activity_model test = new Activity_model();
            test.Name = "Task flan flanya";
            test.Date = "06 Apr, 2018";
            response.Add(test);
            SetUpRecyclerView(recyclerView);

        }
        private async void SetUpRecyclerView(RecyclerView recyclerView)
        {

            recyclerView.SetLayoutManager(new LinearLayoutManager(recyclerView.Context));
            RecyclerView.Adapter adapter = new ActivityRecyclerViewAdapter(recyclerView.Context, response, Resources);
            recyclerView.SetAdapter(adapter);


            recyclerView.SetItemClickListener((rv, position, view) =>
            {
                    //An item has been clicked
                Context context = view.Context;
                Intent intent = new Intent(context, typeof(Detail_Appointment));
                intent.PutExtra(Detail_Lead.EXTRA_NAME, response[position].Name);

                intent.PutExtra("tmp_obj", JsonConvert.SerializeObject(response[position]));
                context.StartActivity(intent);
            });
            
        }

    }
}
